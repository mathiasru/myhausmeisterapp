package buildingstructure;

import exceptions.InvalidFloorException;
import exceptions.InvalidInputException;

import java.io.Serializable;

/**
 * Die Klasse implementiert einen Raum, welcher einem Stockwerk zugeordnet sein muss.
 *
 * @author mathiasrudig
 * @version 1.0
 */
public class Raum implements Serializable {
    public static final int ANZAHL_ZEICHEN_NUMMER = 4;
    public static final int ANZAHL_ZEICHEN_BEZEICHNUNG = 16;
    private String nummer;
    private String bezeichnung;
    private Stockwerk stockwerk;

    /**
     * Konstruktor, welcher beim Erzeugen von Objekten im Fehlerfall Exceptions(InvalidFloorException,
     * InvalidInputException) ausgelöst.
     *
     * @param nummer      übergebene Nummer des Raumes vom Typ String.
     * @param bezeichnung übergebene Bezeichnung des Raumes vom Typ String.
     * @param stockwerk   übergebenes Stockwerk vom Typ Stockwerk.
     * @throws InvalidFloorException beim Übergeben von ungültigen Stockwerken vom Typ Stockwerk.
     * @throws InvalidInputException beim Übergeben einer ungültigen Bezeichnung vom Typ String.
     */
    public Raum(String nummer, String bezeichnung, Stockwerk stockwerk) throws InvalidFloorException, InvalidInputException {
        this.setNummer(nummer);
        this.setBezeichnung(bezeichnung);
        this.setStockwerk(stockwerk);
    }

    /**
     * Getter-Methode, liefert die Nummer des Raumes zurück.
     *
     * @return Liefert die Nummer eines Raumes vom Typ String zurück.
     */
    public String getNummer() {
        return nummer;
    }

    /**
     * Setter-Methode, über die die Nummer eines Raumes gesetzt wird und bei Übergabe eines ungültigen
     * Strings(maximale Anzahl an Zeichen) eine Exception(InvalidInputException) auslöst.
     *
     * @param nummer übergebene Nummer des Raumes vom Typ String.
     * @throws InvalidInputException beim Übergeben einer ungültigen Bezeichnung vom Typ String.
     */
    public void setNummer(String nummer) throws InvalidInputException {
        if (nummer != null && nummer.length() <= ANZAHL_ZEICHEN_NUMMER) {
            this.nummer = nummer;
        } else {
            throw new InvalidInputException("Ungueltige Eingabe, max. " + ANZAHL_ZEICHEN_NUMMER + " Zeichen");
        }
    }

    /**
     * Getter-Methode, liefert die Beschreibung des Raumes zurück.
     *
     * @return Liefert die Beschreibung eines Raumes vom Typ String zurück.
     */
    public String getBezeichnung() {
        return bezeichnung;
    }

    /**
     * Setter-Methode, über die die Beschreibung eines Raumes gesetzt wird und bei Übergabe eines ungültigen
     * Strings(maximale Anzahl an Zeichen) eine Exception(InvalidInputException) auslöst.
     *
     * @param bezeichnung übergebene Bezeichnung des Raumes vom Typ String.
     * @throws InvalidInputException beim Übergeben einer ungültigen Bezeichnung vom Typ String.
     */
    public void setBezeichnung(String bezeichnung) throws InvalidInputException {
        if (nummer != null && nummer.length() <= ANZAHL_ZEICHEN_BEZEICHNUNG) {
            this.bezeichnung = bezeichnung;
        } else {
            throw new InvalidInputException("Ungueltige Eingabe, max. " + ANZAHL_ZEICHEN_BEZEICHNUNG + " Zeichen");
        }
    }

    /**
     * Getter-Methode, liefert das zugewiesene Stockwerk des Raumes zurück.
     *
     * @return Liefert das zugewiesene Stockwerk eines Raumes vom Typ Stockwerk zurück.
     */
    public Stockwerk getStockwerk() {
        return stockwerk;
    }

    /**
     * Setter-Methode, über die das Stockwerk eines Raumes gesetzt wird und bei Übergabe eines ungültigen
     * Stockwerks eine Exception(InvalidFloorException) auslöst.
     *
     * @param stockwerk übergebenes Stockwerk vom Typ Stockwer.
     * @throws InvalidFloorException beim Übergeben von ungültigen Stockwerken vom Typ Stockwerk.
     */
    public void setStockwerk(Stockwerk stockwerk) throws InvalidFloorException {
        if (stockwerk != null) {
            this.stockwerk = stockwerk;
        } else {
            throw new InvalidFloorException();
        }
    }

    /**
     * Die Methode liefert eine Zeichenkette der Datenfelder zurück.
     *
     * @return Liefert einen String der Datenfelder zurück.
     */
    @Override
    public String toString() {
        return String.format("%" + (Raum.ANZAHL_ZEICHEN_NUMMER + Raum.ANZAHL_ZEICHEN_BEZEICHNUNG + 1) +
                "s|%" + Stockwerk.ANZAHL_ZEICHEN_BEZEICHNUNG +
                "s", this.nummer + " " + this.bezeichnung, stockwerk);
    }

}
