package buildingstructure;

import exceptions.InvalidRoomException;
import interference.SammlungStoerungen;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Die Klasse implementiert eine Sammlung von Räumen, welche über Methoden erweitert respektive um Einträge
 * verkleinert werden kann.
 *
 * @author mathiasrudig
 * @version 1.0
 */
public class RaumListe implements Serializable {
    //Datenfelder
    private List<Raum> raumListe;

    /**
     * Konstruktor erzeugt eine neue Sammlung von Räumen in Form einer ArrayList.
     */
    public RaumListe() {
        this.raumListe = new ArrayList<>();
    }

    /**
     * Die Methode implementiert das Hinzufügen von Räumen, welche auf Gültigkeit(darf nur einmal vorkommen)
     * geprüft werden, ansonsten wird eine Exception(InvalidRoomException) ausgelöst.
     *
     * @param raum übergebene Raum vom Typ Raum.
     * @throws InvalidRoomException beim Übergeben von ungültigen Räumen vom Typ Raum.
     */
    public void hinzufuegen(Raum raum) throws InvalidRoomException {
        if (raum != null && !raumListe.contains(raum)) {
            raumListe.add(raum);
        } else {
            throw new InvalidRoomException();
        }
    }

    /**
     * Die Methode implementiert das Entfernen von Räumen, welche in der bestehenden Liste vorhanden sein müssen,
     * ansonsten wird eine Exception(InvalidRoomException) ausgelöst.
     *
     * @param raum übergebene Raum vom Typ Raum.
     * @throws InvalidRoomException beim Übergeben von ungültigen Räumen vom Typ Raum.
     */
    public void entfernen(Raum raum) throws InvalidRoomException {
        if (raum != null && raumListe.contains(raum)) {
            raumListe.remove(raum);
        } else {
            throw new InvalidRoomException();
        }
    }

    /**
     * Die Methode implementiert eine Ausgabe der gesamten Liste aller Räume.
     */
    public void ausgeben() {
        int index = 1;
        System.out.println(tabellenkopfToString());
        System.out.println(tabellenkopfTrennstrichToString());
        for (Raum raeume : raumListe) {
            if (raeume != null) {
                System.out.printf("%" + SammlungStoerungen.ANZAHL_ZEICHEN_INDEXNUMMER + "s|", index);
                System.out.println(raeume);
                System.out.println(tabellenkopfTrennstrichToString());
            }
            index++;
        }
    }

    /**
     * Die Methode liefert eine Liste von Räumen zurück.
     *
     * @return Liefert eine Liste von Räumen vom Typ Raum zurück.
     */
    public List<Raum> getRaumListe() {
        return raumListe;
    }

    /**
     * Die Methode liefert eine formatierte Zeichenkette, welche den Tabellenkopf beinhaltet zurück.
     *
     * @return liefert den formatierten Tabellenkopf vom Typ String zurück.
     */
    private String tabellenkopfToString() {
        return String.format("%" + SammlungStoerungen.ANZAHL_ZEICHEN_INDEXNUMMER +
                "s|%" + (Raum.ANZAHL_ZEICHEN_NUMMER + Raum.ANZAHL_ZEICHEN_BEZEICHNUNG + 1) +
                "s|%" + Stockwerk.ANZAHL_ZEICHEN_BEZEICHNUNG +
                "s", "NR", "Raum", "Stockwerk");
    }

    /**
     * Die Methode liefert einen Trennstrich dessen Länge von der Anzahl der definierten Zeichen abhängt,
     * für die Ausgabe der Tabelle zurück.
     *
     * @return liefert einen Trennstrich vom Typ String zurück.
     */
    private String tabellenkopfTrennstrichToString(){
        String trennstrich = "";
        int laengeTrennstrich = SammlungStoerungen.ANZAHL_ZEICHEN_INDEXNUMMER + (Raum.ANZAHL_ZEICHEN_NUMMER + Raum.ANZAHL_ZEICHEN_BEZEICHNUNG + 1) + Stockwerk.ANZAHL_ZEICHEN_BEZEICHNUNG;
        for (int i = 0; i < laengeTrennstrich+2; i++) { //Die Addition mit der Zahl 2 setzt sich aus der Anzahl der vertikalen Trennstriche zusammen
            trennstrich += "-";
        }
        return trennstrich;
    }
}
