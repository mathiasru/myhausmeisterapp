package com.company;

import buildingstructure.RaumListe;
import buildingstructure.StockwerkListe;
import exceptions.RetrieveDataException;
import exceptions.SaveDataException;
import interference.SammlungStoerungen;
import ui.GlobalContext;
import ui.Kommandozeilenmenue;
import utility.DataStore;
import utility.FileDataStore;

public class HausmeisterApp {
    public static final String GLOBAL_SAMMLUNG_STOERUNGEN = "sammlungStoerungen";
    public static final String GLOBAL_RAUM_LISTE = "raumliste";
    public static final String GLOBAL_STOCKWERK_LISTE = "stockwerkliste";
    private static final DataStore DATA_STORE = new FileDataStore();

    public static void main(String[] args) {
        //initialisieren eines neuen Kommandozeilenmenüs und aufrufen des Hauptmenüs
        Kommandozeilenmenue kommandozeilenmenue = new Kommandozeilenmenue();
        kommandozeilenmenue.hauptmenue();
    }

    //Speichern und wiederherstelen der Daten

    /**
     * Die Methode ermöglicht das Speichern der aktuellen Sitzung über die jeweiligen Methoden und Schreiben in eine Binärdatei.
     */
    public static void speichernDerEintraege() {
        SammlungStoerungen sammlungStoerungen = (SammlungStoerungen) GlobalContext.getGlobalContext().getStateFor(HausmeisterApp.GLOBAL_SAMMLUNG_STOERUNGEN);
        RaumListe raumListe = (RaumListe) GlobalContext.getGlobalContext().getStateFor(HausmeisterApp.GLOBAL_RAUM_LISTE);
        StockwerkListe stockwerkListe = (StockwerkListe) GlobalContext.getGlobalContext().getStateFor(HausmeisterApp.GLOBAL_STOCKWERK_LISTE);

        try {
            speichernSammlungStoerung(sammlungStoerungen);
        } catch (SaveDataException saveDataException) {
            System.out.println("Die SammlungStoerung konnte nicht in die Datei geschrieben werden.");
            System.out.println(saveDataException.getMessage());
        }

        try {
            speichernRaumliste(raumListe);
        } catch (SaveDataException saveDataException) {
            System.out.println("Die RaumListe konnte nicht in die Datei geschrieben werden.");
            System.out.println(saveDataException.getMessage());
        }

        try {
            speichernStockwerkliste(stockwerkListe);
        } catch (SaveDataException saveDataException) {
            System.out.println("Die Stockwerkliste konnte nicht in die Datei geschrieben werden.");
            System.out.println(saveDataException.getMessage());
        }
    }

    /**
     * Die Methode implementiert das Speichern einer SammlungStoerungen in eine Binärdatei.
     *
     * @throws SaveDataException wird bei einem Schreibfehler geschmissen.
     */
    public static void speichernSammlungStoerung(SammlungStoerungen sammlungStoerungen) throws SaveDataException {
        DATA_STORE.speichernSammlungStoerungen(sammlungStoerungen);
        System.out.println("Die SammlungStoerung wurde gespeichert.");
    }

    /**
     * Die Methode implementiert das Speichern einer Raumliste in eine Binärdatei.
     *
     * @throws SaveDataException wird bei einem Schreibfehler geschmissen.
     */
    public static void speichernRaumliste(RaumListe raumListe) throws SaveDataException {
        DATA_STORE.speichernRaumListe(raumListe);
        System.out.println("Die RaumListe wurde gespeichert.");
    }

    /**
     * Die Methode implementiert das Speichern einer Stockwerkliste in eine Binärdatei.
     *
     * @throws SaveDataException wird bei einem Schreibfehler geschmissen.
     */
    public static void speichernStockwerkliste(StockwerkListe stockwerkListe) throws SaveDataException {
        DATA_STORE.speichernStockwerkliste(stockwerkListe);
        System.out.println("Die Stockwerkliste gespeichert.");
    }

    /**
     * Die Methode ermöglicht das Wiederhestellen einer gespeicherten Sitzung un ließt diese ein.
     */
    public static void wiederherstellenDerEintraege() {
        SammlungStoerungen sammlungStoerungen = new SammlungStoerungen();
        RaumListe raumliste = new RaumListe();
        StockwerkListe stockwerkListe = new StockwerkListe();

        try {
            sammlungStoerungen = wiederherstellenSammlungStoerung();
        } catch (RetrieveDataException retrieveDataException) {
            System.out.println("Fehler beim Laden der SammlungStoerung Datei. Es wird eine Leere Liste verwendet!");
            retrieveDataException.printStackTrace();
        }

        try {
            raumliste = wiederherstellenRaumliste();
        } catch (RetrieveDataException retrieveDataException) {
            System.out.println("Fehler beim Laden der Raumliste Datei. Es wird eine Leere Liste verwendet!");
            retrieveDataException.printStackTrace();
        }

        try {
            stockwerkListe = wiederherstellenStockwerkliste();
        } catch (RetrieveDataException retrieveDataException) {
            System.out.println("Fehler beim Laden der Stockwerkliste Datei. Es wird eine Leere Liste verwendet!");
            retrieveDataException.printStackTrace();
        }
        //Dummy-Dateien generieren
        //DummyDateien.dummyDateienGenerieren(sammlungStoerungen, stockwerkListe, raumliste);

        //In den GlobalContext speichern
        GlobalContext.getGlobalContext().putStateFor(HausmeisterApp.GLOBAL_SAMMLUNG_STOERUNGEN, sammlungStoerungen);
        GlobalContext.getGlobalContext().putStateFor(HausmeisterApp.GLOBAL_RAUM_LISTE, raumliste);
        GlobalContext.getGlobalContext().putStateFor(HausmeisterApp.GLOBAL_STOCKWERK_LISTE, stockwerkListe);
    }


    /**
     * Die Methode implementiert das Auslesen einer geschpeicherten SammlungStoerung aus einer Binärdatei und liefert
     * diese dann zurück.
     *
     * @return Liefert eine ausgelesene Sammlung Störung vom Typ SammlungStoerung zurück.
     * @throws RetrieveDataException wird bei einem LeseFehler geschmissen.
     */
    public static SammlungStoerungen wiederherstellenSammlungStoerung() throws RetrieveDataException {
        SammlungStoerungen sammlungStoerungen = DATA_STORE.auslesenSammlungStoerung();
        System.out.println("Die SammlungStoerung wurde geladen.");
        return sammlungStoerungen;
    }

    /**
     * Die Methode implementiert das Auslesen einer geschpeicherten RaumListe aus einer Binärdatei und liefert
     * diese dann zurück.
     *
     * @return Liefert eine ausgelesene RaumListe vom Typ RaumListe zurück.
     * @throws RetrieveDataException wird bei einem LeseFehler geschmissen.
     */
    public static RaumListe wiederherstellenRaumliste() throws RetrieveDataException {
        RaumListe raumliste = DATA_STORE.auslesenRaumListe();
        System.out.println("Die Raumliste wurde geladen.");
        return raumliste;
    }

    /**
     * Die Methode implementiert das Auslesen einer geschpeicherten StockwerkListe aus einer Binärdatei und liefert
     * diese dann zurück.
     *
     * @return Liefert eine ausgelesene StockwerkListe vom Typ StockwerkListe zurück.
     * @throws RetrieveDataException wird bei einem LeseFehler geschmissen.
     */
    public static StockwerkListe wiederherstellenStockwerkliste() throws RetrieveDataException {
        StockwerkListe stockwerkListe = DATA_STORE.auslesenStockwerkListe();
        System.out.println("Die StockwerkListe wurde geladen.");
        return stockwerkListe;
    }


}
