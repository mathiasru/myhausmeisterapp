package interference;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
 * Die ENUM-Klasse implementiert die verschiedenen Gewerke inkl. Beschreibung und eine Liste davon.
 * Die Angelegten ENUMs können beliebig erweitert werden, nur sollte die Anzahl an Zeichen, welche über eine
 * statische Variable(ANZAHL_ZEICHEN_GEWERK) definiert wird, eingehalten, oder der Variablenwert angepasst werden.
 *
 * @author mathiasrudig
 * @version 1.0
 */
public enum Gewerke implements Serializable {
    //ENUMs
    ELEKTRO("Elektro"),
    HLK("HLK"),
    ALLGEMEIN("Allgemein");

    //Datenfelder
    public final String LABEL; //Beschreibung des Gewerkes
    public static final int ANZAHL_ZEICHEN_GEWERK = 10;// die Variable sollte beim erweitern der Klasse beachtet oder
    //ggf. angepasst werden
    public static final int ANZAHL_ZEICHEN_INDEXNUMMER = 3;
    private static final List<Gewerke> GEWERKE_LIST = new ArrayList<>();

    /**
     * Konstruktor setzt die Beschreibung des Gewerkes.
     *
     * @param LABEL übergebene Beschreibung vom Typ String.
     */
    Gewerke(String LABEL) {
        this.LABEL = LABEL;
    }

    /**
     * Die Methode liefert die Beschreibung des Gewerkes(ENUM) als String zurück.
     *
     * @return Liefert die Beschreibung des Gewerkes vom Typ String zurück.
     */
    public String getGewerkeBeschreibung() {
        return this.LABEL;
    }

    /**
     * Die Methode füllt eine Liste mit eindeutigen Einträgen an Gewerken.
     */
    static {
        if (!GEWERKE_LIST.contains(Gewerke.values())) {
            for (Gewerke statusStoerung : Gewerke.values()) {
                GEWERKE_LIST.add(statusStoerung);
            }
        }
    }

    /**
     * Die statische Methode liefert eine Kopie einer Liste von Gewerken zurück.
     *
     * @return Liefert eine gibt eine Kopie einer Liste vom Typ Gewerke zurück.
     */
    public static List<Gewerke> getGewerkeList() {
        return List.copyOf(GEWERKE_LIST);
    }

    /**
     * Die statische Methode gibt eine formatierte Ausgabe einer Liste von Gewerken aus.
     */
    public static void gewerkeListAusgeben() {
        int index = 1;
        System.out.println(tabellenkopfToString());
        System.out.println(tabellenkopfTrennstrichToString());
        for (Gewerke gewerke : Gewerke.getGewerkeList()) {
            System.out.printf("%" + ANZAHL_ZEICHEN_INDEXNUMMER + "s|", index);
            System.out.printf("%" + ANZAHL_ZEICHEN_GEWERK + "s\n", gewerke.LABEL);
            System.out.println(tabellenkopfTrennstrichToString());
            index++;
        }
    }

    /**
     * Die Methode liefert eine formatierte Zeichenkette für den Tabellenkopf zurück.
     *
     * @return Liefert einen formatierten String zurück.
     */
    private static String tabellenkopfToString() {
        return String.format("%" + ANZAHL_ZEICHEN_INDEXNUMMER +
                "s|%" + ANZAHL_ZEICHEN_GEWERK +
                "s", "NR", "Gewerk");
    }
    /**
     * Die Methode liefert einen Trennstrich dessen Länge von der Anzahl der definierten Zeichen abhängt,
     * für die Ausgabe der Tabelle zurück.
     *
     * @return liefert einen Trennstrich vom Typ String zurück.
     */
    private static String tabellenkopfTrennstrichToString(){
        String trennstrich = "";
        int laengeTrennstrich = ANZAHL_ZEICHEN_INDEXNUMMER + ANZAHL_ZEICHEN_GEWERK;
        for (int i = 0; i < laengeTrennstrich+1; i++) { //Die Addition mit der Zahl 1 setzt sich aus der Anzahl der vertikalen Trennstriche zusammen
            trennstrich += "-";
        }
        return trennstrich;
    }
}
