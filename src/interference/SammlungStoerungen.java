package interference;

import buildingstructure.Raum;
import buildingstructure.Stockwerk;
import exceptions.InvalidStoerungException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Die Klasse implementiert eine Sammlung von Störungen, welche über Methoden erweitert respektive um Einträge
 * verkleinert werden kann. Zudem stellt sie mehrere Methoden zum Filtern der Ausgaben bereit.
 *
 * @author mathiasrudig
 * @version 1.0
 */
public class SammlungStoerungen implements Serializable {
    //Datenfelder
    public static final int ANZAHL_ZEICHEN_INDEXNUMMER = 3;
    private ArrayList<Stoerung> stoerungListe;

    /**
     * Konstruktor erzeugt eine neue Sammlung von Störungen in Form einer ArrayList.
     */
    public SammlungStoerungen() {
        this.stoerungListe = new ArrayList<>();
    }

    /**
     * Die Methode implementiert das Hinzufügen von Störungen, welche auf Gültigkeit(darf nur einmal vorkommen)
     * geprüft werden, ansonsten wird eine Exception(InvalidStoerungException) ausgelöst.
     *
     * @param stoerung übergebene Störung vom Typ Störung.
     * @throws InvalidStoerungException beim Übergeben von ungültigen Störungen vom Typ Stoerung.
     */
    public void stoerungHinzufuegen(Stoerung stoerung) throws InvalidStoerungException {
        if (stoerung != null && !this.stoerungListe.contains(stoerung)) {
            this.stoerungListe.add(stoerung);
        } else {
            throw new InvalidStoerungException();
        }
    }

    /**
     * Die Methode implementiert das Entfernen von Störungen, welche in der bestehenden Liste vorhanden sein müssen,
     * ansonsten wird eine Exception(InvalidStoerungException) ausgelöst.
     *
     * @param stoerung übergebene Störung vom Typ Störung.
     * @throws InvalidStoerungException beim Übergeben von ungültigen Störungen vom Typ Stoerung.
     */
    public void stoerungEntfernen(Stoerung stoerung) throws InvalidStoerungException {
        if (stoerung != null && this.stoerungListe.contains(stoerung)) {
            this.stoerungListe.remove(stoerung);
        } else {
            throw new InvalidStoerungException();
        }
    }

    /**
     * Die Methode implementiert eine Ausgabe der gesamten Liste aller Störungen.
     */
    public void stoerungsListeGesamtAusgeben() {
        System.out.println(tabellenkopfToString());
        System.out.println(tabellenkopfTrennstrichToString());
        int index = 1;
        for (Stoerung stoerung : this.stoerungListe) {
            if (stoerung != null) {
                System.out.printf("%" + ANZAHL_ZEICHEN_INDEXNUMMER + "s|", index);
                System.out.println(stoerung);
                System.out.println(tabellenkopfTrennstrichToString());
            }
            index++; //steht außerhalb, damit im falle von null-Objekten trotzem itteriert wird
        }
    }

    /**
     * Die Methode implementiert eine gefilterte Ausgabe nach einem bestimmten Status der Störung.
     *
     * @param statusStoerung übergebener Status der Störung vom Typ StatusStoerung.
     */
    public void stoerungsListeNachStatusAusgeben(StatusStoerung statusStoerung) {
        System.out.println(tabellenkopfToString());
        System.out.println(tabellenkopfTrennstrichToString());
        int index = 1;
        for (Stoerung stoerung : this.stoerungListe) {
            if (stoerung != null && stoerung.getStatus().getStatusStoerungBeschreibung().equals(statusStoerung.getStatusStoerungBeschreibung())) {
                System.out.printf("%" + ANZAHL_ZEICHEN_INDEXNUMMER + "s|", index);
                System.out.println(stoerung);
                System.out.println(tabellenkopfTrennstrichToString());
                index++;
            }
        }
    }

    /**
     * Die Methode implementiert eine gefilterte Ausgabe nach einem bestimmten Gewerk der Störung.
     *
     * @param gewaerk übergebenes Gewerk vom Typ Gewerke.
     */
    public void stoerungsListeNachGewerkAusgeben(Gewerke gewaerk) {
        System.out.println(tabellenkopfToString());
        System.out.println(tabellenkopfTrennstrichToString());
        int index = 1;
        for (Stoerung stoerung : this.stoerungListe) {
            if (stoerung != null && stoerung.getGewaerk().getGewerkeBeschreibung().equals(gewaerk.getGewerkeBeschreibung())) {
                System.out.printf("%" + ANZAHL_ZEICHEN_INDEXNUMMER + "s|", index);
                System.out.println(stoerung);
                System.out.println(tabellenkopfTrennstrichToString());
                index++;
            }
        }
    }

    /**
     * Die Methode implementiert eine gefilterte Ausgabe und gibt nur jene Störungen aus, die eine
     * Deadline haben.
     */
    public void stoerungsListeNachDeadlineAusgeben() {
        System.out.println(tabellenkopfToString());
        System.out.println(tabellenkopfTrennstrichToString());
        int index = 1;
        for (Stoerung stoerung : this.stoerungListe) {
            if (stoerung != null && stoerung instanceof StoerungMitDeadline) {
                System.out.printf("%" + ANZAHL_ZEICHEN_INDEXNUMMER + "s|", index);
                System.out.println(stoerung);
                System.out.println(tabellenkopfTrennstrichToString());
                index++;
            }
        }
    }

    /**
     * Die Methode liefert eine Liste von Störungen zurück.
     *
     * @return Liefert eine Liste von Störungen vom Typ Störung zurück.
     */
    public List<Stoerung> getSammlungStoerung() {
        return this.stoerungListe;
    }

    /**
     * Die Methode liefert eine formatierte Zeichenkette, welche den Tabellenkopf beinhaltet zurück.
     *
     * @return liefert den formatierten Tabellenkopf vom Typ String zurück.
     */
    private String tabellenkopfToString() {
        return String.format("%" + ANZAHL_ZEICHEN_INDEXNUMMER +
                "s|%" + Stoerung.ANZAHL_ZEICHEN_TITEL +
                "s|%" + StatusStoerung.ANZAHL_ZEICHEN_STATUS +
                "s|%" + Gewerke.ANZAHL_ZEICHEN_GEWERK +
                "s|%" + Stoerung.ANZAHL_ZEICHEN_BESCHREIBUNG +
                "s|%" + (Raum.ANZAHL_ZEICHEN_NUMMER + Raum.ANZAHL_ZEICHEN_BEZEICHNUNG + 1) +
                "s|%" + Stockwerk.ANZAHL_ZEICHEN_BEZEICHNUNG +
                "s|%" + Stoerung.ANZAHL_ZEICHEN_DATUM_FORMAT_LONG +
                "s|%" + Stoerung.ANZAHL_ZEICHEN_DATUM_FORMAT_LONG +
                "s", "NR", "Titel", "Status", "Gewerk", "Beschreibung", "Raumbezeichnung", "Stockwerk", "Erstelldatum", "Deadline");
    }

    /**
     * Die Methode liefert einen Trennstrich dessen Länge von der Anzahl der definierten Zeichen abhängt,
     * für die Ausgabe der Tabelle zurück.
     *
     * @return liefert einen Trennstrich vom Typ String zurück.
     */
    private String tabellenkopfTrennstrichToString(){
        String trennstrich = "";
        int laengeTrennstrich = ANZAHL_ZEICHEN_INDEXNUMMER + Stoerung.ANZAHL_ZEICHEN_TITEL + StatusStoerung.ANZAHL_ZEICHEN_STATUS + Gewerke.ANZAHL_ZEICHEN_GEWERK
                + Stoerung.ANZAHL_ZEICHEN_BESCHREIBUNG + (Raum.ANZAHL_ZEICHEN_NUMMER + Raum.ANZAHL_ZEICHEN_BEZEICHNUNG + 1) + Stockwerk.ANZAHL_ZEICHEN_BEZEICHNUNG
                + Stoerung.ANZAHL_ZEICHEN_DATUM_FORMAT_LONG + Stoerung.ANZAHL_ZEICHEN_DATUM_FORMAT_LONG;
        for (int i = 0; i < laengeTrennstrich+8; i++) { //Die Addition mit der Zahl 8 setzt sich aus der Anzahl der vertikalen Trennstriche zusammen
            trennstrich += "-";
        }
        return trennstrich;
    }
}
