package interference;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Die ENUM-Klasse implementiert die verschiedenen Arten von Status inkl. Beschreibung und eine Liste davon.
 * Die Angelegten ENUMs können beliebig erweitert werden, nur sollte die Anzahl an Zeichen, welche über eine
 * statische Variable(ANZAHL_ZEICHEN_STATUS) definiert wird, eingehalten, oder der Variablenwert angepasst werden.
 *
 * @author mathiasrudig
 * @version 1.0
 */
public enum StatusStoerung implements Serializable {
    //ENUMs
    OFFEN("Offen"),
    ERLEDIGT("Erledigt"),
    IN_ARBEIT("in Arbeit");

    //Datenfelder
    public final String LABEL; //Beschreibung des Status
    public static final int ANZAHL_ZEICHEN_STATUS = 10; // die Variable sollte beim erweitern der Klasse beachtet oder
    //ggf. angepasst werden
    public static final int ANZAHL_ZEICHEN_INDEXNUMMER = 3;
    private static List<StatusStoerung> STATUS_STOERUNG_LIST = new ArrayList<>();

    /**
     * Konstruktor setzt die Beschreibung der StatusStoerung.
     *
     * @param LABEL übergebene Beschreibung vom Typ String.
     */
    StatusStoerung(String LABEL) {
        this.LABEL = LABEL;
    }

    /**
     * Die Methode liefert die Beschreibung der StatusStoerung(ENUM) als String zurück.
     *
     * @return Liefert die Beschreibung der StatusStoerung vom Typ String zurück.
     */
    public String getStatusStoerungBeschreibung() {
        return this.LABEL;
    }

    /**
     * Die Methode füllt eine Liste mit eindeutigen Einträgen an StatusStörung.
     */
    static {
        if (!STATUS_STOERUNG_LIST.contains(StatusStoerung.values())) {
            for (StatusStoerung statusStoerung : StatusStoerung.values()) {
                STATUS_STOERUNG_LIST.add(statusStoerung);
            }
        }
    }

    /**
     * Die statische Methode liefert eine Kopie einer Liste von StatusStörung zurück.
     *
     * @return Liefert eine gibt eine Kopie einer Liste vom Typ StatusStörung zurück.
     */
    public static List<StatusStoerung> getStatusStoerungenList() {
        return List.copyOf(STATUS_STOERUNG_LIST);
    }

    /**
     * Die statische Methode gibt eine formatierte Ausgabe einer Liste von StatusStörung aus.
     */
    public static void statusStoerungenListAusgeben() {
        int index = 1;
        System.out.println(tabellenkopfToString());
        System.out.println(tabellenkopfTrennstrichToString());
        for (StatusStoerung statusStoerung : STATUS_STOERUNG_LIST) {
            System.out.printf("%" + ANZAHL_ZEICHEN_INDEXNUMMER + "s|", index);
            System.out.printf("%" + ANZAHL_ZEICHEN_STATUS + "s\n", statusStoerung.LABEL);
            System.out.println(tabellenkopfTrennstrichToString());
            index++;
        }
    }

    /**
     * Die Methode liefert eine formatierte Zeichenkette für den Tabellenkopf zurück.
     *
     * @return Liefert einen formatierten String zurück.
     */
    private static String tabellenkopfToString() {
        return String.format("%" + ANZAHL_ZEICHEN_INDEXNUMMER +
                "s|%" + ANZAHL_ZEICHEN_STATUS +
                "s", "NR", "Status");
    }

    /**
     * Die Methode liefert einen Trennstrich dessen Länge von der Anzahl der definierten Zeichen abhängt,
     * für die Ausgabe der Tabelle zurück.
     *
     * @return liefert einen Trennstrich vom Typ String zurück.
     */
    private static String tabellenkopfTrennstrichToString(){
        String trennstrich = "";
        int laengeTrennstrich = ANZAHL_ZEICHEN_INDEXNUMMER + ANZAHL_ZEICHEN_STATUS;
        for (int i = 0; i < laengeTrennstrich+1; i++) { //Die Addition mit der Zahl 1 setzt sich aus der Anzahl der vertikalen Trennstriche zusammen
            trennstrich += "-";
        }
        return trennstrich;
    }

}
