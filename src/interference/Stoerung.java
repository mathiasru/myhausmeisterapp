package interference;

import buildingstructure.Raum;
import buildingstructure.Stockwerk;
import exceptions.InvalidInputException;
import exceptions.InvalidRoomException;
import exceptions.InvalidStatusException;
import exceptions.InvalidSubsectionsException;
import utility.DateBuilder;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.GregorianCalendar;

/**
 * Die Klasse implementiert eine Störung und implementiert das Marker-Interface Serializable, um die erzeugten Objekte
 * in eine Binärdatei speichern respektive laden zu können. Die Kllasse stellt mehrere Klassenvariablen für Abfragen
 * beziehungsweise Formatierungen bereit.
 *
 * @author mathiasrudig
 * @version 1.0
 */
public class Stoerung implements Serializable {
    //Datenfelder
    public static final DateFormat DATUM_FORMAT_LONG = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT); // Beispiel 14.04.12, 21:34
    public static final int ANZAHL_ZEICHEN_DATUM_FORMAT_LONG = 20;
    public static final int ANZAHL_ZEICHEN_TITEL = 20;
    public static final int ANZAHL_ZEICHEN_BESCHREIBUNG = 32;
    private String titel;
    private String beschreibung;
    private Gewerke gewaerk;
    private Raum raum;
    private StatusStoerung status;
    private final GregorianCalendar erstellungsdatum;

    /**
     * Der Konstruktor ruft beim jeweiligen setzen der Datenfelder immer die jeweilige Setter-Methode auf, um
     * die übergebenen Werte prüfen zu können. Das Erstellungsdatum wird mit einem aktuellen Zeitstempel gesetzt
     * und ist nicht mehr änderbar(final). Zudem müssen die jeweiligen Exceptions(InvalidInputException,
     * InvalidSubsectionsException, InvalidRoomException, InvalidStatusException) beim Erstellen gehandelt werden.
     *
     * @param titel        übergebener Titel vom Typ String
     * @param beschreibung übergebene Beschreibung vom Typ String
     * @param gewaerk      übergebenes Gewerk vom Typ Geweerk
     * @param raum         übergebener Raum vom Typ Raum
     * @param status       übergebener Status vom Typ StatusStoerung
     * @throws InvalidInputException       beim Übergeben einer ungültigen Eingabe Typ String.
     * @throws InvalidSubsectionsException beim Übergeben eines ungültigen Gewerkes vom Typ Gewerk.
     * @throws InvalidRoomException        beim Übergeben eines ungültigen Raumes vom Typ Raum.
     * @throws InvalidStatusException      beim Übergeben von ungültigen Status vom Typ StatusStoerung.
     */
    public Stoerung(String titel, String beschreibung, Gewerke gewaerk, Raum raum, StatusStoerung status) throws InvalidInputException, InvalidSubsectionsException, InvalidRoomException, InvalidStatusException {
        this.setTitel(titel);
        this.setBeschreibung(beschreibung);
        this.setGewaerk(gewaerk);
        this.setRaum(raum);
        this.setStatus(status);
        this.erstellungsdatum = DateBuilder.getAktuellesDatum();
    }

    /**
     * Getter-Methode, welche den Titel zurückliefert.
     *
     * @return Liefert den Titel vom Typ String zurück.
     */
    public String getTitel() {
        return titel;
    }

    /**
     * Setter Methode, über die ein Titel der Störung gesetzt wird und bei Übergabe eines ungueltigen
     * Strings(maximale Anzahl an Zeichen) eine Exception(InvalidInputException) auslöst.
     *
     * @param titel übergebene Beschreibung vom Typ String
     * @throws InvalidInputException beim Übergeben eines ungültigen Titels vom Typ String.
     */
    public void setTitel(String titel) throws InvalidInputException {
        if (titel != null && titel.length() <= ANZAHL_ZEICHEN_TITEL) {
            this.titel = titel;
        } else {
            throw new InvalidInputException("Ungueltige Eingabe, max. " + ANZAHL_ZEICHEN_TITEL + " Zeichen");
        }
    }

    /**
     * Getter-Methode, welche die Beschreibung zurückliefert.
     *
     * @return Liefert die Beschreibung vom Typ String zurück.
     */
    public String getBeschreibung() {
        return beschreibung;
    }

    /**
     * Setter Methode, über die eine Beschreibung der Störung gesetzt wird und bei Übergabe eines ungueltigen
     * Strings(maximale Anzahl an Zeichen) eine Exception(InvalidInputException) auslöst.
     *
     * @param beschreibung übergebene Beschreibung vom Typ String.
     * @throws InvalidInputException beim Übergeben einer ungültigen Beschreibung vom Typ String.
     */
    public void setBeschreibung(String beschreibung) throws InvalidInputException {
        if (titel != null && titel.length() <= ANZAHL_ZEICHEN_BESCHREIBUNG) {
            this.beschreibung = beschreibung;
        } else {
            throw new InvalidInputException("Ungueltige Eingabe, max. " + ANZAHL_ZEICHEN_BESCHREIBUNG + " Zeichen");
        }
    }

    /**
     * Getter-Methode, welche das Gewerk zurückliefert.
     *
     * @return Liefert das Gewerk vom Typ Gewerk zurück.
     */
    public Gewerke getGewaerk() {
        return gewaerk;
    }

    /**
     * Setter Methode, über die ein Gewerk der Störung gesetzt wird und bei Übergabe eines ungueltigen Gewerkes
     * eine Exception(InvalidSubsectionsException) auslöst.
     *
     * @param gewaerk übergebenes Gewerk vom Typ Geweerk
     * @throws InvalidSubsectionsException beim Übergeben eines ungültigen Gewerkes vom Typ Gewerk.
     */
    public void setGewaerk(Gewerke gewaerk) throws InvalidSubsectionsException {
        if (gewaerk != null) {
            this.gewaerk = gewaerk;
        } else {
            throw new InvalidSubsectionsException();
        }
    }

    /**
     * Getter-Methode, welche den Raum zurückliefert.
     *
     * @return Liefert den Raum vom Typ Raum zurück.
     */
    public Raum getRaum() {
        return raum;
    }

    /**
     * Setter Methode, über die ein Raum der Störung gesetzt wird und bei Übergabe eines ungueltigen Raumes
     * eine Exception(InvalidRoomException) auslöst.
     *
     * @param raum übergebener Raum vom Typ Raum.
     * @throws InvalidRoomException beim Übergeben eines ungültigen Raumes vom Typ Raum.
     */
    public void setRaum(Raum raum) throws InvalidRoomException {
        if (raum != null) {
            this.raum = raum;
        } else {
            throw new InvalidRoomException();
        }
    }

    /**
     * Getter-Methode, welche den Status zurückliefert.
     *
     * @return Liefert den Status vom Typ StatusStoerung zurück.
     */
    public StatusStoerung getStatus() {
        return status;
    }

    /**
     * Setter Methode, über die der Status der Störung gesetzt wird und bei Übergabe eines ungültigem Status
     * ein Exception(InvalidStatusException) auslöst.
     *
     * @param status übergebene Status vom Typ StatusStoerung
     * @throws InvalidStatusException beim Übergeben von ungültigen Status vom Typ StatusStoerung.
     */
    public void setStatus(StatusStoerung status) throws InvalidStatusException {
        if (status != null) {
            this.status = status;
        } else {
            throw new InvalidStatusException();
        }
    }

    /**
     * Getter-Methode, welche das Erstellungsdatum zurückliefert.
     *
     * @return Liefert das Erstellungsdatum vom Typ GregorianCalendar zurück
     */
    public GregorianCalendar getErstellungsdatum() {
        return erstellungsdatum;
    }


    /**
     * Die Methode liefert eine formatierte Zeichenkette der Datenfelder zurück.
     *
     * @return Liefert einen formatierten String der Datenfelder zurück.
     */
    @Override
    public String toString() {
        return String.format("%" + Stoerung.ANZAHL_ZEICHEN_TITEL +
                "s|%" + StatusStoerung.ANZAHL_ZEICHEN_STATUS +
                "s|%" + Gewerke.ANZAHL_ZEICHEN_GEWERK +
                "s|%" + Stoerung.ANZAHL_ZEICHEN_BESCHREIBUNG +
                "s|%" + (Raum.ANZAHL_ZEICHEN_NUMMER + Raum.ANZAHL_ZEICHEN_BEZEICHNUNG + 1) +
                "s|%" + Stockwerk.ANZAHL_ZEICHEN_BEZEICHNUNG +
                "s|%" + Stoerung.ANZAHL_ZEICHEN_DATUM_FORMAT_LONG +
                "s|", this.titel, this.status.LABEL, this.gewaerk.LABEL, this.beschreibung, this.raum.getNummer() + " " + this.raum.getBezeichnung(), this.raum.getStockwerk(), DATUM_FORMAT_LONG.format(this.erstellungsdatum.getTime()));
    }

}
