package ui;

import com.company.HausmeisterApp;
import buildingstructure.RaumListe;
import buildingstructure.StockwerkListe;
import interference.SammlungStoerungen;

/**
 * Das Interface stellt default Methoden bereit, welche das Zurückliefern der Der Inhalte des Global Contexts
 * ermöglichen.
 *
 * @author mathiasrudig
 * @version 1.0
 */
public interface GlobalContextState {
    /**
     * Die Methode liefert die aktuelle SammlungStoerungen das dem Global Context zurück.
     *
     * @return liefert die aktuelle SammlungStoerungen vom Typ SammlungStoerungen zurück.
     */
    default SammlungStoerungen getSammlungStoerungen() {
        SammlungStoerungen sammlungStoerungen = (SammlungStoerungen) GlobalContext.getGlobalContext().getStateFor(HausmeisterApp.GLOBAL_SAMMLUNG_STOERUNGEN);
        return sammlungStoerungen;
    }

    /**
     * Die Methode liefert die aktuelle RaumListe das dem Global Context zurück.
     *
     * @return liefert die aktuelle RaumListe vom Typ RaumListe zurück.
     */
    default RaumListe getRaumliste() {
        RaumListe raumListe = (RaumListe) GlobalContext.getGlobalContext().getStateFor(HausmeisterApp.GLOBAL_RAUM_LISTE);
        return raumListe;
    }

    /**
     * Die Methode liefert die aktuelle StockwerkListe das dem Global Context zurück.
     *
     * @return liefert die aktuelle StockwerkListe vom Typ StockwerkListe zurück.
     */
    default StockwerkListe getStockwerkListe() {
        StockwerkListe stockwerkListe = (StockwerkListe) GlobalContext.getGlobalContext().getStateFor(HausmeisterApp.GLOBAL_STOCKWERK_LISTE);
        return stockwerkListe;
    }
}

