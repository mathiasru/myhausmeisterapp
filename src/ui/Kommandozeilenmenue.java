package ui;

import com.company.HausmeisterApp;
import buildingstructure.Raum;
import buildingstructure.Stockwerk;
import exceptions.*;
import interference.Gewerke;
import interference.StatusStoerung;
import interference.Stoerung;
import interference.StoerungMitDeadline;
import utility.DateBuilder;
import utility.InputChecker;

import java.util.GregorianCalendar;
import java.util.Scanner;

/**
 * Die Klasse implementiert das Kommandozeilenmenü der HausmeisterApp.
 *
 * @author mathiasrudig
 * @version 1.0
 */
public class Kommandozeilenmenue implements GlobalContextState {
    //Datenfelder
    private static final Scanner scan = new Scanner(System.in);

    public Kommandozeilenmenue() {
        HausmeisterApp.wiederherstellenDerEintraege();
    }

    /**
     * Startmenue
     */
    public void hauptmenue() {
        String hauptmenue = "-";
        while (!hauptmenue.equals("x")) {
            KommandozeilenmenueTexte.ausgabeHauptmenueAnzeigen();
            KommandozeilenmenueTexte.ausgabeEingabeaufforderung();
            hauptmenue = scan.next();
            switch (hauptmenue) {
                case "1": //Alle Störungen anzeigen
                    try {
                        getSammlungStoerungen().stoerungsListeGesamtAusgeben();
                    } catch (NullPointerException nullPointerException) {
                        System.out.println(nullPointerException.getMessage());
                    }
                    break;
                case "2": //Alle Störungen eines bestimmten Gewerkes anzeigen
                    getSammlungStoerungen().stoerungsListeNachGewerkAusgeben(eingabeStoerungGewerk());
                    break;
                case "3": // Alle Störungen eines bestimmten Status anzeigen
                    getSammlungStoerungen().stoerungsListeNachStatusAusgeben(eingabeStoerungStatus());
                    break;
                case "4": // Alle Störungen mit einer Deadline ausgeben
                    this.getSammlungStoerungen().stoerungsListeNachDeadlineAusgeben();
                    break;
                case "5": // Neue Störung hinzufügen
                    hinzufuegenMenue();
                    break;
                case "6": // Bestehende Störung ändern
                    aendernMenue();
                    break;
                case "7": // Bestehende Störung löschen
                    entfernenMenue();
                    break;
                case "8": // Aktuellen Stand speichern
                    HausmeisterApp.speichernDerEintraege();
                    break;
                case "x": // Menü schließen
                    HausmeisterApp.speichernDerEintraege(); // Aktuellen Stand speichern
                    break;
                default:
                    KommandozeilenmenueTexte.ausgabeFehlerEingabe();
                    break;
            }
        }
        scan.close();
    }

    /**
     * Menue um eine neue Einträge hinzufuegen zu koennen.
     */
    public void hinzufuegenMenue() {
        String untermenue = " ";
        while (!untermenue.equals("x")) {
            KommandozeilenmenueTexte.ausgabeHinzufuegenMenueAnzeige();
            KommandozeilenmenueTexte.ausgabeEingabeaufforderung();
            untermenue = scan.next();
            switch (untermenue) {

                case "1": //Störung hinzufügen
                    if (getRaumliste().getRaumListe().size() > 0) {
                        neueStoerungAnlegen();
                    } else {
                        System.out.println("Kein Raum zum Anlegen einer Störung vorhanden");
                    }
                    break;
                case "2": //Störung mit Deadline hinzufügen
                    if (getRaumliste().getRaumListe().size() > 0) {
                        neueStoerungMitDeadlineAnlegen();
                    } else {
                        System.out.println("Kein Raum zum Anlegen einer Störung vorhanden");
                    }
                    break;
                case "3": //Raum hinzufügen
                    if (getStockwerkListe().getStockwerkListe().size() > 0) {
                        neuenRaumAnlegen();
                    } else {
                        System.out.println("Kein Stockwerk zum Anlegen eines Raumes vorhanden");
                    }
                    break;
                case "4": //Stockwerk hinzufügen
                    neuesStockwerkAnlegen();
                    break;
                case "x": // Menü schließen
                    break;
                default:
                    KommandozeilenmenueTexte.ausgabeFehlerEingabe();
                    break;
            }
        }
    }

    /**
     * Menue um eine neue Einträge hinzufuegen zu koennen.
     */
    public void aendernMenue() {
        String untermenue;
        Stoerung stoerung = eingabeStoerungIndex();
        KommandozeilenmenueTexte.ausgabeAendernStoerungMenueAnzeige();
        KommandozeilenmenueTexte.ausgabeEingabeaufforderung();
        untermenue = scan.next();
        switch (untermenue) {
            case "1": //Titel Störung ändern
                aendernStoerungTitel(stoerung);
                break;
            case "2": //Beschreibung Störung ändern
                aendernStoerungBeschreibung(stoerung);
                break;
            case "3": //Gewerk Störung ändern
                aendernStoerungGewerk(stoerung);
                break;
            case "4": //Raum Störung ändern
                aendernStoerungRaum(stoerung);
                break;
            case "5": //Status Störung ändern
                aendernStoerungStatus(stoerung);
                break;
            case "6": //Deadline Störung ändern
                aendernStoerungDeadline(stoerung);
                break;
            case "x": // Menü schließen
                break;
            default:
                KommandozeilenmenueTexte.ausgabeFehlerEingabe();
                break;
        }
    }

    /**
     * Menue um Einträge entfernen zu koennen.
     */
    public void entfernenMenue() {
        String untermenue = " ";
        while (!untermenue.equals("x")) {
            KommandozeilenmenueTexte.ausgabeEntfernenMenueAnzeige();
            KommandozeilenmenueTexte.ausgabeEingabeaufforderung();
            untermenue = scan.next();
            switch (untermenue) {
                case "1": //Störung entfernen
                    entfernenStoerung();
                    break;
                case "2": //Raum entfernen
                    entfernenRaum();
                    break;
                case "3": //Stockwerk entfernen
                    entfernenStockwerk();
                    break;
                case "x": // Menü schließen
                    break;
                default:
                    KommandozeilenmenueTexte.ausgabeFehlerEingabe();
                    break;
            }
        }
    }

    /**
     * Die Methode implementiert das Löschen von Störungen aus der SammlungStoerung.
     */
    public void entfernenStoerung() {
        try {
            if (getSammlungStoerungen().getSammlungStoerung().size() > 0) {
                getSammlungStoerungen().stoerungEntfernen(eingabeStoerungIndex());
            } else {
                System.out.println("Keine Einträge vorhanden");
            }
        } catch (InvalidStoerungException invalidStoerungException) {
            System.out.println(invalidStoerungException.getMessage());
        }
    }

    /**
     * Die Methode implementiert das Löschen von Räumen aus der Raumliste.
     */
    public void entfernenRaum() {
        try {
            if (getRaumliste().getRaumListe().size() > 0) {
                getRaumliste().entfernen(eingabeStoerungRaum());
            } else {
                System.out.println("Keine Einträge vorhanden");
            }
        } catch (InvalidRoomException invalidRoomException) {
            System.out.println(invalidRoomException.getMessage());
        }
    }

    /**
     * Die Methode implementiert das Löschen von Stockwerken aus der Stockwerksliste.
     */
    public void entfernenStockwerk() {
        try {
            if (getStockwerkListe().getStockwerkListe().size() > 0) {
                getStockwerkListe().entfernen(eingabeRaumStockwerk());
            } else {
                System.out.println("Keine Einträge vorhanden");
            }
        } catch (InvalidFloorException invalidFloorException) {
            System.out.println(invalidFloorException.getMessage());
        }
    }

    /**
     * Die Methode implementiert das Anlegen einer neuen Störung und fügt sie der SammlungStoerung hinzu.
     */
    public void neueStoerungAnlegen() {
        try {
            getSammlungStoerungen().stoerungHinzufuegen(new Stoerung(eingabeStoerungTitel(), eingabeStoerungBeschreibung(), eingabeStoerungGewerk(), eingabeStoerungRaum(), eingabeStoerungStatus()));
        } catch (InvalidInputException | InvalidStatusException | InvalidSubsectionsException | InvalidRoomException | InvalidStoerungException exception) {
            System.out.println(exception.getMessage());
        }
    }

    /**
     * Die Methode implementiert das Anlegen einer neuen Störung mit Deadline und fügt sie der SammlungStoerung hinzu.
     */
    public void neueStoerungMitDeadlineAnlegen() {
        try {
            getSammlungStoerungen().stoerungHinzufuegen(new StoerungMitDeadline(eingabeStoerungTitel(), eingabeStoerungBeschreibung(), eingabeStoerungGewerk(), eingabeStoerungRaum(), eingabeStoerungStatus(), eingabeStoerungDeadlineDatum()));
        } catch (InvalidInputException | InvalidDateException | InvalidStatusException | InvalidSubsectionsException | InvalidRoomException | InvalidStoerungException exception) {
            System.out.println(exception.getMessage());
        }
    }

    /**
     * Die Methode implementiert das Anlegen eines neuen Raumes und fügt sie der Raumliste hinzu.
     */
    public void neuenRaumAnlegen() {
        try {
            getRaumliste().hinzufuegen(new Raum(eingabeRaumNummer(), eingabeRaumBezeichnung(), eingabeRaumStockwerk()));
        } catch (InvalidRoomException | InvalidFloorException | InvalidInputException exception) {
            System.out.println(exception.getMessage());
        }
    }

    /**
     * Die Methode implementiert das Anlegen eines neuen Stockwerkes und fügt sie der Stockwerksliste hinzu.
     */
    public void neuesStockwerkAnlegen() {
        try {
            getStockwerkListe().hinzufuegen(new Stockwerk(eingabeStockwerkBezeichnung()));
        } catch (InvalidFloorException | InvalidInputException exception) {
            System.out.println(exception.getMessage());
        }
    }

    //Eingaben Störungen

    /**
     * Die Methode fordert den Benutzer auf einen Index der SammlungStoerung einzugeben, prüft diesen auf Gültigkeit
     * und liefert die Eingabe als Stoerung zurück.
     *
     * @return Liefert eine gültige Störung vom Typ Stoerung zurück.
     */
    public Stoerung eingabeStoerungIndex() {
        getSammlungStoerungen().stoerungsListeGesamtAusgeben();
        KommandozeilenmenueTexte.ausgabeStrich();
        System.out.println("Geben Sie bitte den Index der gewünschten Störung ein");
        return getSammlungStoerungen().getSammlungStoerung().get(InputChecker.getWertInnerhalbGueltigemBereich(1, getSammlungStoerungen().getSammlungStoerung().size()) - 1);
    }

    /**
     * Die Methode fordert den Benutzer auf ein Titel einzugeben, prüft diesen auf Gültigkeit
     * und liefert die Eingabe als String zurück.
     *
     * @return Liefert einen gültigen Titel vom Typ String zurück.
     */
    public String eingabeStoerungTitel() {
        System.out.println("Geben Sie bitte den Titel der Störung ein");
        return InputChecker.getStringMitGueltigerLaenge(Stoerung.ANZAHL_ZEICHEN_TITEL);
    }

    /**
     * Die Methode fordert den Benutzer auf eine Beschreibung einzugeben, prüft diesen auf Gültigkeit
     * und liefert die Eingabe als String zurück.
     *
     * @return Liefert eine gültige Beschreibung vom Typ String zurück.
     */
    public String eingabeStoerungBeschreibung() {
        System.out.println("Geben Sie bitte die Beschreibung der Störung ein");
        return InputChecker.getStringMitGueltigerLaenge(Stoerung.ANZAHL_ZEICHEN_BESCHREIBUNG);
    }

    /**
     * Die Methode fordert den Benutzer auf einen Index der GewerkeListe einzugeben, prüft diesen auf Gültigkeit
     * und liefert die Eingabe als Gewerke zurück.
     *
     * @return Liefert ein gültiges Gewerk vom Typ Gewerke zurück.
     */
    public Gewerke eingabeStoerungGewerk() {
        Gewerke.gewerkeListAusgeben();
        System.out.println("Geben Sie den Index des gewünschten Gewerkes ein");
        return Gewerke.getGewerkeList().get(InputChecker.getWertInnerhalbGueltigemBereich(1, Gewerke.getGewerkeList().size()) - 1);
    }

    /**
     * Die Methode fordert den Benutzer auf einen Index der Raumliste einzugeben, prüft diesen auf Gültigkeit
     * und liefert die Eingabe als Raum zurück.
     *
     * @return Liefert einen gültigen Raum vom Typ Raum zurück.
     */
    public Raum eingabeStoerungRaum() {
        getRaumliste().ausgeben();
        System.out.println("Geben Sie den Index des gewünschten Raumes ein");
        return getRaumliste().getRaumListe().get(InputChecker.getWertInnerhalbGueltigemBereich(1, getRaumliste().getRaumListe().size()) - 1);
    }

    /**
     * Die Methode fordert den Benutzer auf einen Index der StoerungStatusListe einzugeben, prüft diesen auf Gültigkeit
     * und liefert die Eingabe als StatusStoerung zurück.
     *
     * @return Liefert einen gültigen Status vom Typ StatusStoerung zurück.
     */
    public StatusStoerung eingabeStoerungStatus() {
        StatusStoerung.statusStoerungenListAusgeben();
        System.out.println("Geben Sie den Index des gewünschten Status ein");
        return StatusStoerung.getStatusStoerungenList().get(InputChecker.getWertInnerhalbGueltigemBereich(1, StatusStoerung.getStatusStoerungenList().size()) - 1);
    }

    /**
     * Die Methode fordert den Benutzer auf ein Datum der Deadline einzugeben, prüft diesen auf Gültigkeit
     * und liefert die Eingabe als GregorianCalendar zurück.
     *
     * @return Liefert eine gültiges Datum in der Zukunft vom Typ GregorianCalendar zurück.
     */
    public GregorianCalendar eingabeStoerungDeadlineDatum() {
        return DateBuilder.getDeadlineDatum();
    }

    //Eingaben Räume

    /**
     * Die Methode fordert den Benutzer auf eine Raumnummer einzugeben, prüft diesen auf Gültigkeit
     * und liefert die Eingabe als String zurück.
     *
     * @return Liefert eine gültige Raumnummer vom Typ String zurück.
     */
    public String eingabeRaumNummer() {
        System.out.println("Geben Sie bitte die Raumnummer ein");
        return InputChecker.getStringMitGueltigerLaenge(Raum.ANZAHL_ZEICHEN_NUMMER);
    }

    /**
     * Die Methode fordert den Benutzer auf eine Raumbezeichnung einzugeben, prüft diesen auf Gültigkeit
     * und liefert die Eingabe als String zurück.
     *
     * @return Liefert eine gültige Raumbezeichnung vom Typ String zurück.
     */
    public String eingabeRaumBezeichnung() {
        System.out.println("Geben Sie bitte die Raumbezeichnung ein");
        return InputChecker.getStringMitGueltigerLaenge(Raum.ANZAHL_ZEICHEN_BEZEICHNUNG);
    }

    /**
     * Die Methode fordert den Benutzer auf einen Index der Stockwerksliste einzugeben, prüft diesen auf Gültigkeit
     * und liefert die Eingabe als Stockwerk zurück.
     *
     * @return Liefert ein gültiges Stockwerk vom Typ Stockwerk zurück.
     */
    public Stockwerk eingabeRaumStockwerk() {
        getStockwerkListe().ausgeben();
        System.out.println("Geben Sie den Index des gewünschten Stockwerkes ein");
        return getStockwerkListe().getStockwerkListe().get(InputChecker.getWertInnerhalbGueltigemBereich(1, getStockwerkListe().getStockwerkListe().size()) - 1);
    }

    //Eingabe Stockwerke

    /**
     * Die Methode fordert den Benutzer auf einen Stockwerksbezeichnung einzugeben, prüft diesen auf Gültigkeit
     * und liefert die Eingabe als String zurück.
     *
     * @return Liefert eine gültige Stockwerksbezeichnung vom Typ String zurück.
     */
    public String eingabeStockwerkBezeichnung() {
        System.out.println("Geben Sie bitte die Stockwerksbezeichnung ein");
        return InputChecker.getStringMitGueltigerLaenge(Stockwerk.ANZAHL_ZEICHEN_BEZEICHNUNG);
    }

    //Änderungen von Störungen

    /**
     * Die Methode ermöglicht eine Änderung des Titels einer vorhandenen Störung.
     */
    public void aendernStoerungTitel(Stoerung stoerung) {
        try {
            getSammlungStoerungen().getSammlungStoerung().get(getSammlungStoerungen().getSammlungStoerung().indexOf(stoerung)).setTitel(eingabeStoerungTitel());
        } catch (InvalidInputException invalidInputException) {
            System.out.println(invalidInputException.getMessage());
        }
    }

    /**
     * Die Methode ermöglicht eine Änderung der Beschreibung einer vorhandenen Störung.
     */
    public void aendernStoerungBeschreibung(Stoerung stoerung) {
        try {
            getSammlungStoerungen().getSammlungStoerung().get(getSammlungStoerungen().getSammlungStoerung().indexOf(stoerung)).setBeschreibung(eingabeStoerungBeschreibung());
        } catch (InvalidInputException invalidInputException) {
            System.out.println(invalidInputException.getMessage());
        }
    }

    /**
     * Die Methode ermöglicht eine Änderung des Gewerkes einer vorhandenen Störung.
     */
    public void aendernStoerungGewerk(Stoerung stoerung) {
        try {
            getSammlungStoerungen().getSammlungStoerung().get(getSammlungStoerungen().getSammlungStoerung().indexOf(stoerung)).setGewaerk(eingabeStoerungGewerk());
        } catch (InvalidSubsectionsException exception) {
            System.out.println(exception.getMessage());
        }
    }

    /**
     * Die Methode ermöglicht eine Änderung des Raumes einer vorhandenen Störung.
     */
    public void aendernStoerungRaum(Stoerung stoerung) {
        try {
            getSammlungStoerungen().getSammlungStoerung().get(getSammlungStoerungen().getSammlungStoerung().indexOf(stoerung)).setRaum(eingabeStoerungRaum());
        } catch (InvalidRoomException invalidRoomException) {
            invalidRoomException.printStackTrace();
        }
    }

    /**
     * Die Methode ermöglicht eine Änderung des Status einer vorhandenen Störung.
     */
    public void aendernStoerungStatus(Stoerung stoerung) {
        try {
            getSammlungStoerungen().getSammlungStoerung().get(getSammlungStoerungen().getSammlungStoerung().indexOf(stoerung)).setStatus(eingabeStoerungStatus());
        } catch (InvalidStatusException invalidStatusException) {
            System.out.println(invalidStatusException.getMessage());
        }
    }

    /**
     * Die Methode ermöglicht eine Änderung der Deadline einer vorhandenen Störung.
     */
    public void aendernStoerungDeadline(Stoerung stoerung) {
        try {
            int index = getSammlungStoerungen().getSammlungStoerung().indexOf(stoerung);
            if (getSammlungStoerungen().getSammlungStoerung().get(index) instanceof StoerungMitDeadline) {
                ((StoerungMitDeadline) getSammlungStoerungen().getSammlungStoerung().get(index)).setDeadline(eingabeStoerungDeadlineDatum());
            } else {
                System.out.println("Es wurde keine Störung mit einer Deadline ausgewählt");
            }
        } catch (InvalidDateException invalidDateException) {
            System.out.println(invalidDateException.getMessage());
        }
    }
}
