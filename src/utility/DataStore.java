package utility;

import exceptions.RetrieveDataException;
import exceptions.SaveDataException;
import buildingstructure.RaumListe;
import buildingstructure.StockwerkListe;
import interference.SammlungStoerungen;

/**
 * Das Interfaces DataStore, stellt Methoden zum Speichern und Auslesen von Objekten der
 * Klasse bereit.
 *
 * @author mathiasrudig
 * @version 1.0
 */
public interface DataStore {
    //speichern
    void speichernRaumListe(RaumListe raumliste) throws SaveDataException;
    void speichernStockwerkliste(StockwerkListe stockwerkListe) throws SaveDataException;
    void speichernSammlungStoerungen(SammlungStoerungen sammlungStoerungen) throws SaveDataException;

    //auslesen
    RaumListe auslesenRaumListe() throws RetrieveDataException;
    StockwerkListe auslesenStockwerkListe() throws RetrieveDataException;
    SammlungStoerungen auslesenSammlungStoerung() throws RetrieveDataException;
}
