package utility;

import exceptions.RetrieveDataException;
import exceptions.SaveDataException;
import buildingstructure.RaumListe;
import buildingstructure.StockwerkListe;
import interference.SammlungStoerungen;

import java.io.*;

/**
 * Die Klasse ist für das Speichern und Auslesen in/aus Binärdateien von bestehenden Objekten zuständig und
 * implementiert die Methoden vom Interface DataStore.Wichtig dabei ist, dass die betroffenen Klassen das
 * Marker-Interface Serializable implmentieren um das Speichern/Auslesen realisieren zu können.
 *
 * @author mathiasrudig
 * @version 1.0
 */
public class FileDataStore implements DataStore {
    //Dateinamen
    private static final String DATEINAME_RAUMLISTE = "./exportFiles/raumListe.bin";
    private static final String DATEINAME_STOCKWERKLISTE = "./exportFiles/stockwerkListe.bin";
    private static final String DATEINAME_SAMMLUNG_STOERUNGEN = "./exportFiles/sammlungStoerungen.bin";

    /**
     * Die Methode implementiert das Speichern von übergebenen Objekten vom Typ RaumListe und speichert diese
     * als Binärdatei ab.
     *
     * @param raumliste übergebener RaumListe vom Typ RaumListe
     * @throws SaveDataException Fehler beim Schreiben in eine Binärdatei.
     */
    @Override
    public void speichernRaumListe(RaumListe raumliste) throws SaveDataException {
        if (raumliste != null) {
            //Die Klasse ObjectOutputStream ermöglicht das Speichern von Objekten in eineBinärdatei über die Hilfsklasse FileOutput.
            ObjectOutputStream objectOutputStream;
            try {
                objectOutputStream = new ObjectOutputStream(new FileOutputStream(DATEINAME_RAUMLISTE));
                //Schreiben des übergebenen Objektes in Datei
                objectOutputStream.writeObject(raumliste);
                objectOutputStream.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
                throw new SaveDataException("Fehler beim Speichern der Raumliste: " + ioException.getMessage());
            }
        }
    }

    /**
     * Die Methode implementiert das Speichern von übergebenen Objekten vom Typ StockwerkListe und speichert diese
     * als Binärdatei ab.
     *
     * @param stockwerkListe übergebener StockwerkListe vom Typ StockwerkListe
     * @throws SaveDataException Fehler beim Schreiben in eine Binärdatei.
     */
    @Override
    public void speichernStockwerkliste(StockwerkListe stockwerkListe) throws SaveDataException {
        if (stockwerkListe != null) {
            //Die Klasse ObjectOutputStream ermöglicht das Speichern von Objekten in eineBinärdatei über die Hilfsklasse FileOutput.
            ObjectOutputStream objectOutputStream;
            try {
                objectOutputStream = new ObjectOutputStream(new FileOutputStream(DATEINAME_STOCKWERKLISTE));
                //Schreiben des übergebenen Objektes in Datei
                objectOutputStream.writeObject(stockwerkListe);
                objectOutputStream.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
                throw new SaveDataException("Fehler beim Speichern der StockwerkListe: " + ioException.getMessage());
            }
        }
    }

    /**
     * Die Methode implementiert das Speichern von übergebenen Objekten vom Typ SammlungStoerungen und speichert diese
     * als Binärdatei ab.
     *
     * @param sammlungStoerungen übergebener SammlungStoerungen vom Typ SammlungStoerungen
     * @throws SaveDataException Fehler beim Schreiben in eine Binärdatei.
     */
    @Override
    public void speichernSammlungStoerungen(SammlungStoerungen sammlungStoerungen) throws SaveDataException {
        if (sammlungStoerungen != null) {
            //Die Klasse ObjectOutputStream ermöglicht das Speichern von Objekten in eineBinärdatei über die Hilfsklasse FileOutput.
            ObjectOutputStream objectOutputStream;
            try {
                objectOutputStream = new ObjectOutputStream(new FileOutputStream(DATEINAME_SAMMLUNG_STOERUNGEN));
                //Schreiben des übergebenen Objektes in Datei
                objectOutputStream.writeObject(sammlungStoerungen);
                objectOutputStream.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
                throw new SaveDataException("Fehler beim Speichern der SammlungStoerung: " + ioException.getMessage());
            }
        }
    }

    /**
     * Die Methode implementiert das Auslesen von Objekten vom Typ RaumListe aus einer Binärdatei und gibt diese
     * dann zurück.
     *
     * @return gibt einen RaumListe von Typ RaumListe zurück
     * @throws RetrieveDataException Fehler beim Auslesen aus der Binärdatei.
     */
    @Override
    public RaumListe auslesenRaumListe() throws RetrieveDataException {
        ObjectInputStream objectInputStream;
        try {
            objectInputStream = new ObjectInputStream(new FileInputStream(DATEINAME_RAUMLISTE));
            //Auslesen der Datei und zuweisung an Objekt (hier müsste ggf. auf den Typ gefrüft werden mittels instanceOf)
            RaumListe raumListe = (RaumListe) objectInputStream.readObject();
            objectInputStream.close();
            return raumListe;
        } catch (IOException | ClassNotFoundException exception) {
            exception.printStackTrace();
            throw new RetrieveDataException("Fehler beim Laden der Raumliste: " + exception.getMessage());
        }
    }

    /**
     * Die Methode implementiert das Auslesen von Objekten vom Typ StockwerkListe aus einer Binärdatei und gibt diese
     * dann zurück.
     *
     * @return gibt einen StockwerkListe von Typ StockwerkListe zurück
     * @throws RetrieveDataException Fehler beim Auslesen aus der Binärdatei.
     */
    @Override
    public StockwerkListe auslesenStockwerkListe() throws RetrieveDataException {
        ObjectInputStream objectInputStream;
        try {
            objectInputStream = new ObjectInputStream(new FileInputStream(DATEINAME_STOCKWERKLISTE));
            //Auslesen der Datei und zuweisung an Objekt (hier müsste ggf. auf den Typ gefrüft werden mittels instanceOf)
            StockwerkListe stockwerkListe = (StockwerkListe) objectInputStream.readObject();
            objectInputStream.close();
            return stockwerkListe;
        } catch (IOException | ClassNotFoundException exception) {
            exception.printStackTrace();
            throw new RetrieveDataException("Fehler beim Laden der Stockwerkliste: " + exception.getMessage());
        }
    }

    /**
     * Die Methode implementiert das Auslesen von Objekten vom Typ SammlungStoerungen aus einer Binärdatei und gibt diese
     * dann zurück.
     *
     * @return gibt einen SammlungStoerungen von Typ SammlungStoerungen zurück
     * @throws RetrieveDataException Fehler beim Auslesen aus der Binärdatei.
     */
    @Override
    public SammlungStoerungen auslesenSammlungStoerung() throws RetrieveDataException {
        ObjectInputStream objectInputStream;
        try {
            objectInputStream = new ObjectInputStream(new FileInputStream(DATEINAME_SAMMLUNG_STOERUNGEN));
            //Auslesen der Datei und zuweisung an Objekt (hier müsste ggf. auf den Typ gefrüft werden mittels instanceOf)
            SammlungStoerungen sammlungStoerungen = (SammlungStoerungen) objectInputStream.readObject();
            objectInputStream.close();
            return sammlungStoerungen;
        } catch (IOException | ClassNotFoundException exception) {
            exception.printStackTrace();
            throw new RetrieveDataException("Fehler beim Laden der SammlungStoerung: " + exception.getMessage());
        }
    }
}
